let menu = document.querySelector('#menu-bars');
let nav = document.querySelector('.navbar');
let navLinks = nav.querySelectorAll('a');
let section = document.querySelectorAll('section');
/*console.log(navLinks);*/

menu.onclick = () => {
  menu.classList.toggle('fa-times');
  nav.classList.toggle('active');
}

onscroll = function() {
  var scrollPosition = document.documentElement.scrollTop;
  console.log(scrollPosition)
  section.forEach((sec) => {
    if(scrollPosition >= sec.offsetTop - sec.offsetHeight*0.25 && 
    scrollPosition < sec.offsetTop + sec.offsetHeight - sec.offsetHeight*0.25)
    {
      var id = sec.getAttribute('id');
      removeAllActiveClasses();
      addActiveClass(id);
    }
  });
};

var removeAllActiveClasses = function () {
  navLinks.forEach((link) =>{
    link.classList.remove('active');
  });
}

var addActiveClass = function(id){
  var selector = `header .navbar a[href="#${id}"]`;
  document.querySelector(selector).classList.add('active');
}

navLinks.forEach(link => {
  link.addEventListener('click', function () {
    navLinks.forEach(navlink =>
      navlink.classList.remove('active'))
    this.classList.add('active');
  });
});

var swiper = new Swiper(".container", {
  spaceBetween: 30,
  centeredSlides: true,
  autoplay: {
    delay: 4000,
    disableOnInteraction: false,
  },
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  loop: true,
});

